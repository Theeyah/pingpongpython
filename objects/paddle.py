from pygame import key, Rect, Surface, Vector2, draw, Rect
from pygame.constants import K_UP, K_DOWN
from game_object import GameObject
from objects.ball import Ball

# Définition de la classe Paddle
class Paddle(GameObject):

    # Constructeur de la classe
    def __init__(self, ball: Ball = None, is_player=True):
        # Position de la raquette
        #Rect(x,y,w,h)
        self.rect = Rect(0, 0, 5, 100)
        # La raquette est controlée par le joueur ? O/N
        self.is_player = is_player
        # Quelle est la balle du jeu (pour l'IA)
        self.ball = ball
        # Vitesse de déplacement de la raquette
        self.speed = 1
    # Méthode d'initialisation de l'objet, à exécuter une fois au début
    def init(self, screen: Surface):
        self.screen = screen
        if (self.is_player):
            self.rect.x=10
            self.rect.y=200
        else:
            self.rect.x=780
            self.rect.y=200

    # Méthode de mise à jour de l'objet, à exécuter à chaque image
    def update(self):
        # Il existe une fonction par type de joueur, manuel ou IA
        if (self.is_player):
            self.manual_control()
        else:
            self.automatic_control()

        # Maintenant que les coordonées ont changés, on dessine la raquette
        draw.rect(self.screen, (255, 255, 255), self.rect)

    def manual_control(self):
        # Ici vous mettez votre logique de mouvement, avec les touches de clavier etc.
        # Raquette J1
            if (key.get_pressed()[K_UP]):
                self.rect.y -= self.speed
            if (self.rect.y<0):
                self.rect.y=0
            if (key.get_pressed()[K_DOWN]):
                self.rect.y += self.speed
            if (self.rect.y > 500):
                self.rect.y = 500

    # Imbattable, la raquette suis la balle directement sans faute
    def automatic_control(self):
        self.rect.y = self.ball.pos.y
        if (self.rect.y<0):
            self.rect.y=0
        if (self.rect.y>500):
            self.rect.y=500
